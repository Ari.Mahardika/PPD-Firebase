package com.ridims.ppdfirebase.Helper;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by
 * Name : Ari Mahardika Ahmad Nafis
 * Email : arimahardika.an@gmail.com
 * Git : gitlab.com/Ari.Mahardika
 * Date : 17/10/16
 */

public class Static extends AppCompatActivity {

    private static final String DOSEN_KEY = "NAMADOSEN";
    private static final String JURUSAN_KEY = "NAMAJURUSAN";

    public static String getDosenKey() {
        return DOSEN_KEY;
    }

    public static String getJurusanKey() {
        return JURUSAN_KEY;
    }
}
