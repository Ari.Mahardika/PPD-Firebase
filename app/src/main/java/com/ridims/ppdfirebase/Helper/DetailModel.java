package com.ridims.ppdfirebase.Helper;

/**
 * Created by
 * Name : Ari Mahardika Ahmad Nafis
 * Email : arimahardika.an@gmail.com
 * Git : gitlab.com/Ari.Mahardika
 * Date : 17/10/16
 */

public class DetailModel {
    public String nama;
    public String email;
    public String hp;

    public String alamat;
    public String notelprumah;
    public String ruang;

    public DetailModel() {
    }

    public DetailModel(String nama, String email, String hp, String alamat, String notelprumah, String ruang) {
        this.nama = nama;
        this.email = email;
        this.hp = hp;
        this.alamat = alamat;
        this.notelprumah = notelprumah;
        this.ruang = ruang;
    }
}
